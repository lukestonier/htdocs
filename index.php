<!DOCTYPE html>
<html>
    <?php
        include('Head.php');
    ?>

    <body>
        <?php
            include('Header.php');
        ?>

        <div class="landing-container">
            <div class="landing">
                <div class="overlay">
                    <div class="product-container">
                        <h1 class="title">ULTIMATE COP BOT</h1>
                        <div class="product-image-container">
                            <img src="Images/Bot.png"/>
                        </div>
                        <div class="buy-container">
                            <button class="buy-button">BUY SUBSCRIPTION NOW</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="features">
            <h1 class="heading">FEATURES</h1>
        </div>

        <?php
            include('Footer.php');
        ?>
    </body>
</html>